$(function(){
    $("#logingButten").on("click", function(){
        var username = $("#userName").val();
        var password = $("#password").val();
        login(username, password);
    })

    if(sessionStorage.getItem("logedin") === "ok"){
        hidelogin()
    }

});

login = async function (userName, password) {
    $.ajax({
      url: "http://localhost:9090/api/v1/login",
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Authorization": "Basic " + btoa(userName+ ":" + password),
        "userName": userName
      },
      success: function (userId) {
        sessionStorage.setItem("userName", userName)
        sessionStorage.setItem("userPassword", password)
        sessionStorage.setItem("userId", userId)
        sessionStorage.setItem("logedin", "ok")
        console.log(sessionStorage.getItem("userName"))
        hidelogin();
      },
      
    });
  };
  hidelogin = function(){
    $("#userName").hide();
    $("#password").hide();
    $("#logingButten").hide();
    $(".label").hide();
    $(".para").show();
  }