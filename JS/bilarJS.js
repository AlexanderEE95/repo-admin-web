$(function () {
  getCars();
  $("#updateCarButton").hide();
  $("#sendCarButton").hide();
  $("#sortButton").on("click", function(){
  sortera();
  });
});

getCars = async function () {
  $.ajax({
    url: "http://localhost:9090/api/v1/admin/cars",
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Basic " + btoa(sessionStorage.getItem("userName") + ":" + sessionStorage.getItem("userPassword")),
    },
    success: function (carArray) {
      buildCarTable(carArray);
    },
  });
};

buildCarTable = function (cars) {
  carTable = $("#bilarTBody");
  carTable.html("");
  standardForm(carTable)
  for (var i = 0; i < cars.length; i++) {
    tableRow = $(`<tr class = 'carTableRow'> </tr>`);
    carTable.append(tableRow);

    tableCell = $(`<td class='carTableCell' id='carId'> </td>`);
    tableCell.html(`<button class='updateCar btn funktion' id="${i}">uppdatera</button>`);
    tableRow.append(tableCell);

    tableCell = $(`<td class='carTableCell' id='model'> </td>`);
    tableCell.html(cars[i].modell);
    tableRow.append(tableCell);

    tableCell = $(`<td class='carTableCell' id='name'> </td>`);
    tableCell.html(cars[i].name);
    tableRow.append(tableCell);

    tableCell = $(`<td class='carTableCell' id='PricePerDay'> </td>`);
    tableCell.html(cars[i].pricePerDay);
    tableRow.append(tableCell);

    tableCell = $(`<td class='carTableCell' id='carId'> </td>`);
    tableCell.html(`<button class='removeCar btn negative' id="${i}">tabort</button>`);
    tableRow.append(tableCell);
   
  }

  $( ".updateCar" ).on("click", function(){
    buildUpdateTable(cars[$(this)[0].id]);
  });
  $( ".removeCar" ).on("click", function(){
    deletCar(cars[$(this)[0].id]);
  });

};

buildUpdateTable = function (car) {
  $("#addCarButton").hide();
  $("#updateCarButton").show();
  oldTable = $("#bilarTBody");
  oldTable.html(""); 
  carTable = $("#upTBody");
  carTable.html(""); 

  standardForm(carTable);

  tableRow = $(`<tr class = 'carTableRowUpp'> </tr>`);
  carTable.append(tableRow);
  
  tableCell = $(`<td class='carTableCellUpp' id='carId'> </td>`);
  tableCell.html(`<input type="text" id="carIdText">`);
  tableRow.append(tableCell);

  tableCell = $(`<td class='carTableCellUpp' id='model'> </td>`);
  tableCell.html(`<input type="text" id="carModelText">`);
  tableRow.append(tableCell);

  tableCell = $(`<td class='carTableCellUpp' id='name'> </td>`);
  tableCell.html(`<input type="text" id="carNametext">`);
  tableRow.append(tableCell);

  tableCell = $(`<td class='carTableCellUpp' id='PricePerDay'> </td>`);
  tableCell.html(`<input type="number" id="carPricePerDayNumber">`);
  tableRow.append(tableCell);

  

  $("#carIdText").val(car.id)
  $("#carModelText").val(car.modell)
  $("#carNametext").val(car.name)
  $("#carPricePerDayNumber").val(car.pricePerDay)
};

standardForm = function(carTable){
  //carTable = $("#bilarTBody");
  
    tableRow = $(`<tr class = 'carTableRow'> </tr>`);
    carTable.append(tableRow);

    tableCell = $(`<td class='carTableCell' id='carId'> </td>`);
    tableCell.html("id");
    tableRow.append(tableCell);

    tableCell = $(`<td class='carTableCell' id='modell'> </td>`);
    tableCell.html("modell");
    tableRow.append(tableCell);

    tableCell = $(`<td class='carTableCell' id='name'> </td>`);
    tableCell.html("name");
    tableRow.append(tableCell);

    tableCell = $(`<td class='carTableCell' id='PricePerDay'> </td>`);
    tableCell.html("pris per dag");
    tableRow.append(tableCell);
    tableCell = $(`<td class='carTableCell' id='removeButtenId'> </td>`);
    tableCell.html("");
    tableRow.append(tableCell);

}

$("#updateCarButton").on("click", function(){
  item = {};
  item["id"]=$("#carIdText").val();
  item["modell"]=$("#carModelText").val();
  item["name"]=$("#carNametext").val();
  item["pricePerDay"]=$("#carPricePerDayNumber").val();
  console.log(item);
  updateCar(item);
})

updateCar = async function (car) {
  $.ajax({
    url: "http://localhost:9090/api/v1/updateCar",
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Basic " + btoa(sessionStorage.getItem("userName") + ":" + sessionStorage.getItem("userPassword")),
    },
    data:JSON.stringify(car),
    success: function(){
      console.log("bil uppdaterad")
      $(".display").load("/html/bilar.html");
    }
  });
};

deletCar = async function (car) {
  $.ajax({
    url: "http://localhost:9090/api/v1/deletcar",
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Basic " + btoa(sessionStorage.getItem("userName") + ":" + sessionStorage.getItem("userPassword")),
    },
    data:JSON.stringify(car),
    success: function(){
      console.log("bil bortagen")
      $(".display").load("/html/bilar.html");
    }
  });
};
$("#addCarButton").on("click", function(){
  $("#addCarButton").hide();
  $("#sortButton").hide();
  $("#selectSort").hide();
  $("#sendCarButton").show();


  oldTable = $("#bilarTBody");
  oldTable.html(""); 
  carTable = $("#upTBody");
  carTable.html(""); 
  
  standardForm(carTable);

  tableRow = $(`<tr class = 'carTableRow'> </tr>`);
  carTable.append(tableRow);
  
  tableCell = $(`<td class='carTableCell' id='carId'> </td>`);
  tableCell.html(`<input type="text" id="carIdText">`);
  tableRow.append(tableCell);

  tableCell = $(`<td class='carTableCell' id='model'> </td>`);
  tableCell.html(`<input type="text" id="carModelText">`);
  tableRow.append(tableCell);

  tableCell = $(`<td class='carTableCell' id='name'> </td>`);
  tableCell.html(`<input type="text" id="carNametext">`);
  tableRow.append(tableCell);

  tableCell = $(`<td class='carTableCell' id='PricePerDay'> </td>`);
  tableCell.html(`<input type="number" id="carPricePerDayNumber">`);
  tableRow.append(tableCell);
  
  $("#carIdText").attr("readonly", "readonly");
  $("#carIdText").val("id sätts atomatiskt");

  $("#sendCarButton").on("click", function(){
    item = {};
    item["modell"]=$("#carModelText").val();
    item["name"]=$("#carNametext").val();
    item["pricePerDay"]=$("#carPricePerDayNumber").val();
    addCar(item);
  })

});

addCar = async function (car) {
  $.ajax({
    url: "http://localhost:9090/api/v1/addcar",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Basic " + btoa(sessionStorage.getItem("userName") + ":" + sessionStorage.getItem("userPassword")),
    },
    data:JSON.stringify(car),
    success: function(){
      console.log("bil tillagd")
      $(".display").load("/html/bilar.html");
    }
  });
};

sortera = async function () {
  $.ajax({
    url: "http://localhost:9090/api/v1/admin/cars",
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Basic " + btoa(sessionStorage.getItem("userName") + ":" + sessionStorage.getItem("userPassword")),
    },
    success: function (carArray) {
      sortCars(carArray);
    },
  });
};

sortCars = function(cars){
  if($("#selectSort").val() == 1){
    cars.sort(sortByModell)
  } else if($("#selectSort").val() == 2){
    cars.sort(sortByName)
  } else if($("#selectSort").val() == 3){
    cars.sort(sortByPrice)
  }

  buildCarTable(cars);
}
// sorteing metoder
sortByModell = function(a,b){
  var aName = a.modell.toLowerCase();
  var bName = b.modell.toLowerCase();
  return((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}
sortByName = function(a,b){
  var aName = a.name.toLowerCase();
  var bName = b.name.toLowerCase();
  return((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}
sortByPrice = function(a,b){
  var aprice = a.pricePerDay;
  var bprice = b.pricePerDay;
  return((aprice > bprice ) ? -1 : ((aprice < bprice ) ? 1: 0));
}