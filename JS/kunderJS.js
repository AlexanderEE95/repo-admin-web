$(function () {
  getKunder();
  $("#sortButton").on("click", function(){
    sortList();
  });
  $("#filterButton").on("click", function(){
    filterkunderList();
  });
  $("#returnKunder").on("click", function(){
    getKunder();
  })
});

getKunder = function(){
  $.ajax({
    url: "http://localhost:9090/api/v1/customers",
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Basic " + btoa(sessionStorage.getItem("userName") + ":" + sessionStorage.getItem("userPassword")),
    },
    success: function (customerArray) {
      buildKundeTable(customerArray);
      console.log(customerArray);
    },
  });
}

buildKundeTable = function(kunder){
  kunderTable = $("#kunderTbody");
  kunderTable.html("");

  standartFrom();
  for (var i = 0; i < kunder.length; i++) {
    kunderAndOrderNumbers(kunder[i].id, kunder[i]);
   }
}

standartFrom = function(){
  kunderTable = $("#kunderTbody");
  
    tableRow = $(`<tr class = 'standardRow'> </tr>`);
    kunderTable.append(tableRow);

    tableCell = $(`<td class= 'kunderTableCell' id='userName'> </td>`);
    tableCell.html("Användarnamn");
    tableRow.append(tableCell);

    tableCell = $(`<td class= 'kunderTableCell' id='name'> </td>`);
    tableCell.html("Namn");
    tableRow.append(tableCell); 

    tableCell = $(`<td class= 'kunderTableCell' id='address'> </td>`);
    tableCell.html("Address");
    tableRow.append(tableCell);  

    tableCell = $(`<td class= 'kunderTableCell' id='orders'> </td>`);
    tableCell.html("antal orders");
    tableRow.append(tableCell);
}

kunderAndOrderNumbers = function(id, kund){

  $.ajax({
    url: "http://localhost:9090/api/v1/OrdersOfCustomer",
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Basic " + btoa(sessionStorage.getItem("userName") + ":" + sessionStorage.getItem("userPassword")),
      "id" : id
    },
    success: function (orders){
      buildKundeTablePart2(orders,kund);
    }
    
  });
}

buildKundeTablePart2 = function(count, kund){
  kunderTable = $("#kunderTbody");

  tableRow = $(`<tr class = 'kunderTableRow'> </tr>`);
  kunderTable.append(tableRow);

  tableCell = $(`<td class= 'kunderTableCell' id='userName'> </td>`);
  tableCell.html(kund.userName);
  tableRow.append(tableCell);

  tableCell = $(`<td class= 'kunderTableCell' id='name'> </td>`);
  tableCell.html(kund.name);
  tableRow.append(tableCell); 

  tableCell = $(`<td class= 'kunderTableCell' id='address'> </td>`);
  tableCell.html(kund.address);
  tableRow.append(tableCell);

  tableCell = $(`<td class= 'kunderTableCell' id='orders'> </td>`);
  tableCell.html(count);
  tableRow.append(tableCell);
  
};

filterkunderList = function(){
  var arr = [];
  $(".kunderTableRow").each(function(){
    item = {}
    item["userName"] =  $(this).find("td:eq(0)").text();
    item["name"] =  $(this).find("td:eq(1)").text();
    item["address"] =  $(this).find("td:eq(2)").text();
    item["orders"] =  $(this).find("td:eq(3)").text();
    arr.push(item);
  });

  var removevalue = $("#filterNumber").val();

  arr = $.grep(arr, function(n){
    return n.orders == removevalue;
  })

  buildAfterSort(arr);
}

buildAfterSort = function(kunder){
  kunderTable = $("#kunderTbody");
  kunderTable.html("");

  standartFrom();
  for (var i = 0; i < kunder.length; i++) {
     
  tableRow = $(`<tr class = 'kunderTableRow'> </tr>`);
  kunderTable.append(tableRow);

  tableCell = $(`<td class= 'kunderTableCell' id='userName'> </td>`);
  tableCell.html(kunder[i].userName);
  tableRow.append(tableCell);

  tableCell = $(`<td class= 'kunderTableCell' id='name'> </td>`);
  tableCell.html(kunder[i].name);
  tableRow.append(tableCell); 

  tableCell = $(`<td class= 'kunderTableCell' id='address'> </td>`);
  tableCell.html(kunder[i].address);
  tableRow.append(tableCell);

  tableCell = $(`<td class= 'kunderTableCell' id='orders'> </td>`);
  tableCell.html(kunder[i].orders);
  tableRow.append(tableCell);
}
}

sortList = function(){
    var arr = [];
    $(".kunderTableRow").each(function(){
      item = {}
      item["userName"] =  $(this).find("td:eq(0)").text();
      item["name"] =  $(this).find("td:eq(1)").text();
      item["address"] =  $(this).find("td:eq(2)").text();
      item["orders"] =  $(this).find("td:eq(3)").text();
      arr.push(item);
    });

    if($("#sortSelect").val() == 1){
      arr.sort(sortByUserName);
    }else if ($("#sortSelect").val() == 2){
      arr.sort(sortByName);
    }else if ($("#sortSelect").val() == 3){
      arr.sort(sortByAddres);
    }else if ($("#sortSelect").val() == 4){
      arr.sort(sortByOrdersAsc);
    }
    buildAfterSort(arr);

}

// sorterings metoder

sortByUserName = function(a,b){
  var aUserName = a.userName.toLowerCase();
  var bUserName = b.userName.toLowerCase();
  return((aUserName < bUserName) ? -1 : ((aUserName > bUserName) ? 1 : 0));
}
sortByName = function(a,b){
  var aName = a.name.toLowerCase();
  var bName = b.name.toLowerCase();
  return((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}
sortByAddres = function(a,b){
  var aAddress = a.address;
  var bAddress = b.address;
  return((aAddress < bAddress ) ? -1 : ((aAddress > bAddress ) ? 1: 0));
}
sortByOrdersAsc = function(a,b){
  var aorders = a.orders;
  var borders = b.orders;
  return((aorders > borders ) ? -1 : ((aorders < borders ) ? 1: 0));
}
sortByOrdersDesc = function(a,b){
  var aorders = a.orders;
  var borders = b.orders;
  return((aorders < borders ) ? -1 : ((aorders > borders ) ? 1: 0));
}
